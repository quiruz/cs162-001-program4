#include "plans.h"

// ****************************************************************************
// John Quiruz, August 21 2023, CS162, Program 4
// This file contains the main operation of the program. It manages every
// process that takes place and keeps the data flow organized and concise.
// The main purpose of this program is to keep track of all activities that
// the user wants to do during their Fall break. The program saves information
// such as the title, the location, the cost, and other detailed information
// about the activity and displays the information.
// ****************************************************************************

int main()
{
	//Create class objects
	plan fall_plan;
	plans fall_plans;

	//Add activities to the planner and display them
	fall_plans.get_activities();
	fall_plans.display_all();

	//Ask the user for a keyword to search for specific activity
	fall_plans.find_activity();

	return 0;
}
