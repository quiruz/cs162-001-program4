#include "plan.h"

// ****************************************************************************
// John Quiruz, Aug 21 2023, CS162, Program 4
// This file contains the class interface for the list of plans made for 
// activities for the user to do during the Fall. Includes the member data for
// creating an array of class objects (plans) and keeps track of size of array 
// ****************************************************************************

//This class holds a list of activities
class plans
{
	public:
		plans(); ~plans(); //Constructor and Destructor
		void get_activities(); //Reads in a collection of plans
		void display_all();
		void find_activity();

	private:
		plan *array; //Points to a collection of many activities
		int array_size; //Initializes the size of the array at runtime
				//which is determined by the user
		int num_plans; 
		char *keyword; //Another dynamic array to be used for keyword
			       //search
};
