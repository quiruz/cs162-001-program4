#include <iostream>
#include <cctype>
#include <cstring>
using namespace std;

// ****************************************************************************
// John Quiruz, August 21 2023, CS162, Program 4
// This file is the class interface that contains data members and function
// members that are utilized to populate a collection of activities. We want
// to use a dynamic array to store the name of the activity without using a 
// predetermined value for the size of memory. This file also contains all of
// base level preprocessor directives and constants that the entire program
// may utilize.
// ****************************************************************************

//Constants
const int NAME {61};
const int INFO {101};
const int TIME {11};

//This class holds the information of a single activity
class plan
{
	public:
		plan();	~plan(); //Constructor and Destructor!
		void get_activity_info(); //Read in plans for fall
		void display(); //Display plans for fall
		char *activity; //My dynamic array case

	private:
		char duration[INFO]; //How long the activity will last
		char description[INFO]; //What is this trip about
		char location[INFO]; //Where is  the activity at
		float cost; // Cost of the activity

};
