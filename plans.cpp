#include "plans.h"

// ****************************************************************************
// John Quiruz, Aug 21 2023, CS162, Program 4
// This file contains the implementation code for class functions related to 
// organizing a list of activities that the user is planning on doing during
// Fall Break. Included are all member functions that manipulate the data
// ****************************************************************************

//Must initialize the data members in order to construct 
//the instance of each data
plans::plans()
{
	//Setting the size of a dynamic array based on what the user chooses
	cout << "\nPlease enter the amount of activities "
	     << "that you want to plan: ";
	cin >> array_size;
	cin.ignore(100, '\n');

	array = new plan [array_size + 1]; // This statement literally causes
					   // (array) to point at a newly
					   // allocated dynamic memory
	num_plans = 0;
	keyword = NULL; // Dynamic array
}



//Must deallocate dynamic memory and reset the pointer 
//to null in order to avoid memory issues
plans::~plans()
{
	if (NULL != array)
		delete [] array; //after the program is done, deallocate array
	array = NULL; //don't forget to point array to NULL

	if (NULL != keyword)
		delete [] keyword;
	keyword = NULL;
}



//This contains the planner filled with all activities that the user entered in
void plans::get_activities()
{
	while (num_plans < array_size)
	{
		array[num_plans].get_activity_info();
		++num_plans;
	}
}



//Displays the entire collection
void plans::display_all()
{
	for (int i {0}; i < num_plans; ++i)
	{
		cout << endl;
		array[i].display();
	}
}



//Find the activity in the collection of activities that
//matches the keyword
void plans::find_activity()
{
	//set variables
	char temp[INFO];
	bool match_found {false};
	
	//get keyword from the user and store in a temporary
	cout << "\nEnter the activity that you want to display: ";
	cin.get(temp, INFO, '\n');
	cin.ignore(100, '\n');

	//creates the dynamic array to store the keyword stored in temp in
	keyword = new char [strlen(temp) +1];
	strcpy(keyword, temp);

	//Searches the collection for the activity
	for (int i {0}; i < num_plans && !match_found; ++i)
	{
		if (strcmp(keyword, array[i].activity) == 0)
		{
			match_found = true;
			cout << "Match found! Displaying activity...\n" << endl;
			array[i].display();
		}
	}	
	if (!match_found) //
		cout << "Sorry the activity was not found..." << endl << endl;
}
