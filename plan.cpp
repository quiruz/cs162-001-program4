#include "plans.h"

// ****************************************************************************
// John Quiruz, August 21 2023, CS162, Program 4
// This file contains the implementation code for the function members
// that add, process, and output a single activity.
// ****************************************************************************

//Functions

//Constructor
plan::plan()
{
	activity = NULL;
	duration[0] = '\0';
	description[0] = '\0';
	location[0] = '\0';
	cost = 0;
}



//Destructor that deallocates dynamic memory
plan::~plan()
{
	if (NULL != activity)
		delete [] activity;
	activity = NULL;
}



//This function gets infromation about the activities that the user
//wants to do during Fall break
void plan::get_activity_info()
{
	//Creates a fixed array to store a temporary name
	char temp[INFO];

	//Gets the name and stores it into a temporary array
	cout << "\nEnter the name of the activity: ";
	cin.get(temp, INFO, '\n');
	cin.ignore(100, '\n');
	
	//Make sure that the activity pointer is pointing at NULL before
	//creating a new dynamic array
	activity = new char [strlen(temp) + 1];
	strcpy(activity, temp);

	//Get the rest of the information about the activity
	cout << "Enter the duration of the activity: ";
	cin.get(duration, TIME, '\n');
	cin.ignore(100, '\n');

	cout << "Enter the cost of the activity: ";
	cin >> cost;
	cin.ignore(100, '\n');

	cout << "Enter the description of the activity: ";
	cin.get(description, INFO, '\n');
	cin.ignore(100, '\n');

	cout << "Enter the location of the activity: ";
	cin.get(location, INFO, '\n');
	cin.ignore(100, '\n');
}



//This function desplays all the information that the user has entered
//about what they have planned for Fall 
void plan::display()
{
	cout << "Activity: " << activity << endl
	     << "Cost: $" << cost << endl
	     << "Duration: " << duration << endl
	     << "Description: " << description << endl
	     << "Location: " << location << endl; 
}
